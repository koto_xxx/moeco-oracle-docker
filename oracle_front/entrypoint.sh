#!/bin/sh

rm -y /etc/nginx/nginx.conf

envsubst '' < /nginx.conf.template > /etc/nginx/nginx.conf

nginx -g 'daemon off;'
