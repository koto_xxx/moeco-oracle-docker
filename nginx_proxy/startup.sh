#!/bin/sh

rm -y /etc/nginx/nginx.conf

envsubst '${ORACLE_BACK_HOST},${ORACLE_BACK_PORT},${ORACLE_FRONT_HOST},${ORACLE_FRONT_PORT},${ORACLE_SERVICE_PORT},${ORACLE_SERVICE_HOST}' < /nginx.conf.template > /etc/nginx/nginx.conf

nginx -g 'daemon off;'
