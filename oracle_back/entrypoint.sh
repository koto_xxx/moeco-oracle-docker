#!/bin/sh

set -e

pm2 start --no-daemon server.js
